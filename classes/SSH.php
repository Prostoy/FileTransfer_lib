<?php

namespace FileTransfer;

class SSH implements ConnectionInterface
{
	protected $currentPath;
    protected $connection;
    
    public function __construct($user, $pass, $hostname, $port = 22){ //check $port param
        $this->connection = ssh2_connect($hostname, $port);
        if (!$this->connection) {
            throw new \Exception("Couldn't connect to $hostname:$port");
        }
        if (!ssh2_auth_password($this->connection, $user, $pass)) {
            throw new \Exception("Couldn't authorize with $user:$pass on $hostname:$port");
        }

        if($this->currentPath  = !ssh2_exec($this->connection,'pwd')){
			throw new \Exception("Couldn't define current path");
		}

    }
    
    //TODO is $path exists
    public function cd($path){
        if (!is_string($path)) {
            throw new \InvalidArgumentException('$path should be a string');
        }

        if($this->isFromRoot($path)){
			$this->currentPath = $path;
		}else{
			$this->currentPath .= '/'.$path;
		}

		$this->cleanSlashes($this->currentPath);

        return $this;

    }
    
    public function pwd(){
        $stream = ssh2_exec($this->connection, 'cd '.$this->currentPath.'; '.'pwd');
        if (!$stream) {
            throw new \Exception("Cannot read current directory or exec error");
        }

        stream_set_blocking( $stream, true );
        $response = '';
        while ($line=fgets($stream))
        {
                $response .= $line;
        }
        return $response;
    }
    
    //TODO is file exists
    //TODO is enough space
    public function download($fileName){
        if (!is_string($fileName)) {
            throw new \InvalidArgumentException('$fileName should be a string');
        }

        $localFileName = $fileName;

		$fileName = $this->currentPath.'/'.$fileName;
		$fileName = $this->cleanSlashes($fileName);


        if (!ssh2_scp_recv($this->connection, $fileName, $localFileName)){
            throw new \Exception("Cannot download file $fileName");
        }

        return $this;

    }
    
    //TODO is file exists
    //TODO is enough space
    public function upload($fileName){
        if (!is_string($fileName)) {
            throw new \InvalidArgumentException('$fileName should be a string');
        }

		$localFileName = $fileName;

		$fileName = $this->currentPath.'/'.$fileName;
		$fileName = $this->cleanSlashes($fileName);

        if (!ssh2_scp_send($this->connection, $localFileName, $fileName)){
            throw new \Exception("Cannot upload file $localFileName");
        }
    }
    
    public function exec($command){
        if (!is_string($command)) {
            throw new \InvalidArgumentException('$command should be a string');
        }
        $stream = ssh2_exec($this->connection, 'cd '.$this->currentPath.'; '.$command);
        if (!$stream){
            throw new \Exception("Couldn't execute command $command");
        }
        
        stream_set_blocking($stream, true); 
        $response = '';
        while ($line=fgets($stream)) { 
            $response .= $line; 
        }
        return $response;
    }
    
    public function close(){
        $closeResult = ssh2_exec($this->connection, 'exit;');
        if (!$closeResult) {
            throw new \Exception("Cannot read current directory");
        }
        return $closeResult;
    }


    private function cleanSlashes($path){
		while(strpos($path,'//') !== FALSE){
			$path = str_replace('//','/',$path);
		}

		return $path;

	}

	private function isFromRoot ($path){

		return strpos($path,'/') === 0;

	}

}